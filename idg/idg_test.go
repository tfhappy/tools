package idg

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func initGenerator() {
	err := InitIDG(0)
	if err != nil {
		panic(err)
	}
}

func TestGenerateID(t *testing.T) {
	re := require.New(t)
	initGenerator()
	id := GetID()
	// t.Logf("get id: %#v, len: %d", id, len(id))
	re.Equal(24, len(id))
}

func TestGenerateDuplicateID(t *testing.T) {
	initGenerator()
	ids := make(map[string]bool)
	for i := 0; i < 1000000; i++ {
		id := GetID()
		if ids[id] == true {
			t.Errorf("%s has been generated", id)
			break
		}
		ids[id] = true
	}
}

func TestHasnotInit(t *testing.T) {
	generator = nil
	re := require.New(t)
	defer func() {
		e := recover()
		err, ok := e.(error)
		re.True(ok)
		re.Contains(err.Error(), "runtime error: invalid memory address or nil pointer")
	}()

	GetID()
}

func Benchmark_GetID(b *testing.B) {
	b.StopTimer()
	err := InitIDG(0)
	if err != nil {
		panic(err)
	}
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		GetID()
	}
}
