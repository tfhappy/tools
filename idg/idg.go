package idg

import (
	"encoding/base64"

	"github.com/bwmarrin/snowflake"
)

var (
	generator *snowflake.Node
	Epoch     int64 = 1613551865000 // 2021-02-17 16:51:05
)

// 0<=node<=1023
// 每个进程都应该分配不同的node
func InitIDG(node int64) error {
	snowflake.Epoch = Epoch
	var err error
	generator, err = snowflake.NewNode(node)
	return err
}

func GetID() string {
	id := generator.Generate()
	return base64.URLEncoding.EncodeToString(id.Bytes())
}
