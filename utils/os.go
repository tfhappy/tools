package utils

import (
	"os"
	"path/filepath"
)

// 获取执行文件所在目录的绝对路径
// 注意: 未编译前,执行文件位置在go-build目录下
func MainPWD() (string, error) {
	ex, err := os.Executable()
	if err != nil {
		return "", err
	}

	real, err := filepath.EvalSymlinks(ex)
	if err != nil {
		return "", err
	}
	return filepath.Dir(real), nil
}
