package token

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

func CreateToken(key []byte, userId string, TTL time.Duration) (string, error) {
	now := time.Now()
	claims := &jwt.StandardClaims{
		Id:        userId,
		ExpiresAt: now.Add(TTL).Unix(), // 过期时间
		IssuedAt:  now.Unix(),          // 颁发时间
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(key)
}
