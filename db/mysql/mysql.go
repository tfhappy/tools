package mysql

import (
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var DB *sql.DB

func ConnectMysql(dataSourceName string, maxOpen, maxIdle int, maxLife time.Duration) error {
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		return err
	}

	db.SetConnMaxLifetime(maxLife)
	db.SetMaxOpenConns(maxOpen)
	db.SetMaxIdleConns(maxIdle)

	DB = db

	return DB.Ping()
}
