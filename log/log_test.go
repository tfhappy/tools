package log

import (
	"testing"

	"github.com/sirupsen/logrus"
)

func Test(t *testing.T) {
	logrus.Debug("is debug")
	logrus.Error("is error")
	logrus.Info("is info")
}
