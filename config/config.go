package config

import "time"

type MysqlConfig struct {
	DSN     string
	MaxOpen int
	MaxIdle int
	MaxLife Duration
}

type Duration struct {
	time.Duration
}

func (d *Duration) UnmarshalText(text []byte) error {
	var err error
	d.Duration, err = time.ParseDuration(string(text))
	return err
}
